
import utfpr.ct.dainf.if62c.pratica.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author pozza
 * 
 */
public class Pratica41 {
    
    public static void main(String[] args) {
        System.out.println("Area : "+new Elipse(0.3,0.4).getArea());
        System.out.println("Perimetro : "+new Elipse(0.3,0.4).getPerimetro());
        
        System.out.println("Perimetro : "+new Circulo(1.4).getPerimetro());
    }
    
}
